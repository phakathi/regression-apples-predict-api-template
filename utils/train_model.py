"""
    Simple file to create a Sklearn model for deployment in our API

    Author: Explore Data Science Academy

    Description: This script is responsible for training a simple linear
    regression model which is used within the API for initial demonstration
    purposes.

"""

# Dependencies
import pandas as pd
import pickle
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from sklearn import metrics


# Fetch training data and preprocess for modeling
train = pd.read_csv('data/train_data.csv')

train = train[(train['Commodities'] == 'APPLE GOLDEN DELICIOUS')]

# y = train['avg_price_per_kg']
# X = train[['Weight_Kg','Low_Price','High_Price','Total_Qty_Sold','Total_Kg_Sold','Stock_On_Hand']]

  
# Fit model
lm_regression = LinearRegression(normalize=True)
print ("Training Model...")
lm_regression.fit(X_train, y_train)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size = 0.1,random_state=42)

RF = RandomForestRegressor(n_estimators=500, max_depth=10)
RF.fit(X_train,y_train)

# Pickle model for use within our API
save_path = '../assets/trained-models/model1.pkl'
print (f"Training completed. Saving model to: {save_path}")
pickle.dump(RF, open(save_path,'wb'))
